﻿using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;

namespace udzielanie_rabatu.tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void udzielanie_rabatu()
        {
            string rabat = "10,00";
            decimal cena = 100.00m;
            decimal d_rabat = System.Convert.ToDecimal(rabat);
            rabat = rabat.Replace(',', '.');

            if (d_rabat == 0)
            {
                return;
            }

            decimal cena_po_rabacie = cena - (cena / d_rabat);
            decimal cena_do_sprawdzenia = 90.00m; //Wartość jaka powinna wyjść


            Assert.AreEqual(cena_po_rabacie, cena_do_sprawdzenia);
        }

        [TestMethod()]
        //Sprawdzenie czy dodawany kontrahent istnieje w bazie danych 
        public void dodawanie_nowego_kontrahenta()
        {
            string nazwa = "Karol Pitra";
            string zwrotka = "Karol Pitra";
            bool sprawdzenie;
            SqlConnection con = new SqlConnection(@"Data Source=KAROLEK\KAROL;Initial Catalog=w59018;Integrated Security=true");
            con.Open();
            SqlCommand cmd = new SqlCommand("SELECT nazwa FROM odbiorca WHERE nazwa ='" + nazwa + "'", con);
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
            zwrotka = (string)cmd.ExecuteScalar();

            if (zwrotka == nazwa)
            {
                sprawdzenie = true;
            }
            else
            {
                sprawdzenie = false;
            }

                Assert.IsTrue(sprawdzenie);
        }

       
}
