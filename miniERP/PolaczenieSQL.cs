﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;

namespace miniERP
{
    /// <summary>
    ///  Polaczenie z baza danych SQL.
    /// </summary>
    public class PolaczenieSQL

    {
        public SqlCommand cmd;
        public SqlConnection con;
        public SqlDataAdapter da;




        /// <summary>
        ///  Zadeklarowanie stringa z polaczeniem do bazy danych
        /// </summary>
        public void polaczenieSQL()
        {   
            con = new SqlConnection(@"Data Source=KAROLEK\KAROL;Initial Catalog=w59018;Integrated Security=true");
            con.Open();
        }

        /// <summary>
        /// Metoda odpowiadajaca za zainicjowanie polaczenia
        /// </summary>
        public void startpolaczenie()
        {
            con.Open();
        }

        /// <summary>
        /// Metoda konczaca polaczenia
        /// </summary>
        public void zakpolaczenieSQL()
        {
            con.Close();
        }
    }
}
