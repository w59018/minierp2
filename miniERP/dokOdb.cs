﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;



namespace miniERP
{
    /// <summary>
    ///  Klasa zaiwerająca obsługę kartotek kontrahentów.
    /// </summary>
    public partial class dokOdb : Form
    {
        PolaczenieSQL pol = new PolaczenieSQL();
       
        string id;

        public dokOdb()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Metoda ładująca dane do okna
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void dokOdb_Load(object sender, EventArgs e)
        {
            // TODO: Ten wiersz kodu wczytuje dane do tabeli 'w59018DataSet.odbiorca' . Możesz go przenieść lub usunąć.
            this.odbiorcaTableAdapter.Fill(this.w59018DataSet.odbiorca);
            //listaOdb.Columns["odbiorca_idDataGridViewTextBoxColumn"].Visible = false;
        }

        /// <summary>
        /// Metoda pobierająca dane z aktualnie zaznaczonej pozycji w tabeli
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void fillByToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.odbiorcaTableAdapter.FillBy(this.w59018DataSet.odbiorca);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        /// <summary>
        /// Metoda odświeżająca połączenie z bazą danych
        /// </summary>
        public void odswiezenie()
        {
            pol.polaczenieSQL();            
            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM odbiorca", pol.con);
            DataSet ds = new DataSet();
            da.Fill(ds, "odbiorca");
            listaOdb.DataSource = ds;
            listaOdb.DataMember = "odbiorca";
            pol.con.Close();

        }

        /// <summary>
        /// Metoda dodająca nowego odbiorcę po wciśnięciu przycisku dodaj
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void bDodaj_Click(object sender, EventArgs e)
        {
            if (tbNazwa.Text != "" && tbKod.Text != "" && tbPoczta.Text != "" && tbUlica.Text != "")
                {
                    pol.polaczenieSQL();

                pol.cmd = new SqlCommand("SELECT nazwa FROM odbiorca WHERE nazwa ='" + tbNazwa.Text + "'", pol.con);
                pol.cmd.CommandType = CommandType.Text;
                pol.cmd.ExecuteNonQuery();
                string zwrotka = (string)pol.cmd.ExecuteScalar();

                if (zwrotka == tbNazwa.Text)
                {
                    DialogResult pytanie = MessageBox.Show("Podany kontrahent " + tbNazwa.Text + " już istnieje, czy mimo tego chcesz dodać go ponownie?", "Nowy Kontrahent" , MessageBoxButtons.YesNo);
                    if (pytanie == DialogResult.Yes)
                    {
                        goto DODANIE;
                    }
                    else if (pytanie == DialogResult.No)
                    {
                        return;
                    }
                }



                DODANIE:
                pol.cmd = new SqlCommand("INSERT INTO  odbiorca (nazwa, powiat, gmina, kodpoczt, miasto, poczta, ulica, nrdomu, rabat) " +
                                     "VALUES	(@nazwa,@powiat,@gmina,@kodpoczt,@miasto, @poczta, @ulica, @nrdomu, @rabat)", pol.con);
                    pol.cmd.Parameters.AddWithValue("@nazwa", tbNazwa.Text);
                    pol.cmd.Parameters.AddWithValue("@powiat", tbPowiat.Text);
                    pol.cmd.Parameters.AddWithValue("@kodpoczt", tbKod.Text);
                    pol.cmd.Parameters.AddWithValue("@gmina", tbGmina.Text);
                    pol.cmd.Parameters.AddWithValue("@miasto", tbPoczta.Text);
                    pol.cmd.Parameters.AddWithValue("@poczta", tbPoczta.Text);
                    pol.cmd.Parameters.AddWithValue("@ulica", tbUlica.Text);
                    pol.cmd.Parameters.AddWithValue("@nrdomu", tbNr.Text);

                if (tbRabat.Text == "")
                    {
                        tbRabat.Text = "0";
                    }

                    pol.cmd.Parameters.AddWithValue("@rabat", decimal.Parse(tbRabat.Text));
                    pol.cmd.CommandType = CommandType.Text;
                    pol.cmd.ExecuteNonQuery();

                    string message = "Dodano Nowego Odbiorcę";
                    string caption = "Dodano pozycję";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    DialogResult result;

                    tbNazwa.Text = "";
                    tbKod.Text = "";
                    tbPoczta.Text = "";
                    tbPowiat.Text = "";
                    tbGmina.Text = "";
                    tbRabat.Text = "";
                    tbUlica.Text = "";
                    tbNr.Text = "";

                    result = MessageBox.Show(message, caption, buttons);
                                    
                    odswiezenie();
            }
            else
            {
                string message = "Nie wypełniono wymaganych pól: Imię Nazwisko, Kod, Poczta, Ulica !";
                string caption = "UWAGA";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result;
                result = MessageBox.Show(message, caption, buttons);
                
            }
        }

        /// <summary>
        /// Metoda wywoływana po wciśnięciu przycisku usuń, powoduje usunięcie kontrahenta
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void bUsun_Click(object sender, EventArgs e)
        {
            pol.polaczenieSQL();            
            string query = "DELETE FROM odbiorca WHERE odbiorca_id = " + id;
            pol.cmd = new SqlCommand(query, pol.con);
            pol.cmd.CommandType = CommandType.Text;
            try
            {
                pol.cmd.ExecuteNonQuery();
            }
            catch (SqlException blad)
            {
                if (blad.Errors.Count > 0) 
                {
                        MessageBox.Show("Nie można usunąć odbiorcy, posiada on wystawione faktury sprzedaży!");
                        return;
                }
                
            }


            odswiezenie();
            pol.zakpolaczenieSQL();
            
            
        }

        /// <summary>
        /// Pole z nazwa
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void tbNazwa_TextChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Pole z kodem pocztowym
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void tbKod_TextChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Pole z pocztą
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void tbPoczta_TextChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Pole powiat
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void tbPowiat_TextChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Pole Gmina
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void tbGmina_TextChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Pole rabat 
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void tbRabat_TextChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Pole ulica
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void tbUlica_TextChanged(object sender, EventArgs e)
        {
           
        }

        /// <summary>
        /// Pole Nr
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void tbNr_TextChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Inicjalizowania polaczenia SQL i odswiezenie danych wraz pobraniem id
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            pol.polaczenieSQL();
            odswiezenie();
            DataGridViewRow row = this.listaOdb.Rows[e.RowIndex];
            id = row.Cells["odbiorca_id"].Value.ToString();
            pol.con.Close();
        }

        /// <summary>
        /// Pob ieranie danych z aktualnie zaznaczonej pozycji na liscie
        /// </summary>
        private void listaOdb_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //pol.polaczenieSQL();
            //odswiezenie();
            DataGridViewRow row = this.listaOdb.Rows[e.RowIndex];
            tbNazwa.Text = row.Cells["nazwa"].Value.ToString();
            tbKod.Text = row.Cells["kodpoczt"].Value.ToString();
            tbPoczta.Text = row.Cells["poczta"].Value.ToString();
            tbPowiat.Text = row.Cells["powiat"].Value.ToString();
            tbGmina.Text = row.Cells["gmina"].Value.ToString();
            tbRabat.Text = row.Cells["rabat"].Value.ToString();
            tbUlica.Text = row.Cells["ulica"].Value.ToString();
            tbNr.Text = row.Cells["nrdomu"].Value.ToString();
            id = row.Cells["odbiorca_id"].Value.ToString();
            //pol.con.Close();

        }

        private void fillByToolStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        /// <summary>
        /// Przycisk aktualizujący dane
        /// </summary>
        private void bZmien_Click(object sender, EventArgs e)
        {
            if (tbNazwa.Text != "" && tbKod.Text != "" && tbPoczta.Text != "" && tbUlica.Text != "" && id != "")
            {
                pol.polaczenieSQL();
                string query = "UPDATE odbiorca  SET nazwa = @nazwa, powiat = @powiat, gmina = @gmina, kodpoczt = @kodpoczt, miasto = @miasto, poczta = @poczta, ulica = @ulica, nrdomu = @nrdomu, rabat = @rabat " +
                                 "WHERE odbiorca_id =" + id;
                pol.cmd = new SqlCommand(query, pol.con);
                pol.cmd.Parameters.AddWithValue("@nazwa", tbNazwa.Text);
                pol.cmd.Parameters.AddWithValue("@powiat", tbPowiat.Text);
                pol.cmd.Parameters.AddWithValue("@kodpoczt", tbKod.Text);
                pol.cmd.Parameters.AddWithValue("@gmina", tbGmina.Text);
                pol.cmd.Parameters.AddWithValue("@miasto", tbPoczta.Text);
                pol.cmd.Parameters.AddWithValue("@poczta", tbPoczta.Text);
                pol.cmd.Parameters.AddWithValue("@ulica", tbUlica.Text);
                pol.cmd.Parameters.AddWithValue("@nrdomu", tbNr.Text);
                //MessageBox.Show(query);

                if (tbRabat.Text == "")
                {
                    tbRabat.Text = "0";
                }

                pol.cmd.Parameters.AddWithValue("@rabat", decimal.Parse(tbRabat.Text));
                pol.cmd.CommandType = CommandType.Text;
                pol.cmd.ExecuteNonQuery();

                string message = "Zmieniono Odbiorcę";
                string caption = "Aktualizacja";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result;
                
                result = MessageBox.Show(message, caption, buttons);

                odswiezenie();
            }
            else
            {
                string message = "Zaznacz odbiorcę na liście";
                string caption = "UWAGA";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result;
                result = MessageBox.Show(message, caption, buttons);

            }
        }
    }
}
