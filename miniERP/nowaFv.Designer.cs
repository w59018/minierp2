﻿namespace miniERP
{
    partial class nowaFv
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(nowaFv));
            this.btUsun = new System.Windows.Forms.Button();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbCena = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbUwagi = new System.Windows.Forms.TextBox();
            this.tbIlosc = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dodaj = new System.Windows.Forms.Button();
            this.comboBoxDostawca = new System.Windows.Forms.ComboBox();
            this.tbOpisDokumentu = new System.Windows.Forms.TextBox();
            this.LabelOpisDokumentu = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.LabelRabat = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.doksprzedpoz_temp = new System.Windows.Forms.DataGridView();
            this.doksprzedpozidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wytworidmDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iloscDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cenaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cenaporabacieDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uwagiDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.doksprzedpoztempBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.w59018DataSet2 = new miniERP.w59018DataSet2();
            this.doksprzedpoztempBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.doksprzedpoz_tempTableAdapter = new miniERP.w59018DataSet2TableAdapters.doksprzedpoz_tempTableAdapter();
            this.btAnulujRabat = new System.Windows.Forms.Button();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.bWstecz = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.doksprzedpoz_temp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.doksprzedpoztempBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.w59018DataSet2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.doksprzedpoztempBindingSource)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btUsun
            // 
            this.btUsun.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.btUsun.Location = new System.Drawing.Point(572, 101);
            this.btUsun.Name = "btUsun";
            this.btUsun.Size = new System.Drawing.Size(121, 53);
            this.btUsun.TabIndex = 31;
            this.btUsun.Text = "USUŃ POZYCJE";
            this.btUsun.UseVisualStyleBackColor = true;
            this.btUsun.Click += new System.EventHandler(this.btUsun_Click);
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(154, 66);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(236, 21);
            this.comboBox2.TabIndex = 30;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.button1.Location = new System.Drawing.Point(722, 101);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(275, 53);
            this.button1.TabIndex = 29;
            this.button1.Text = "ZATWIERDŹ DOKUMENT";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(95, 69);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 28;
            this.label5.Text = "Produkt";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(84, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 27;
            this.label4.Text = "Odbiorca";
            // 
            // tbCena
            // 
            this.tbCena.Location = new System.Drawing.Point(315, 101);
            this.tbCena.Name = "tbCena";
            this.tbCena.Size = new System.Drawing.Size(75, 20);
            this.tbCena.TabIndex = 26;
            this.tbCena.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.keypress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(262, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "Cena";
            // 
            // tbUwagi
            // 
            this.tbUwagi.Location = new System.Drawing.Point(154, 134);
            this.tbUwagi.Name = "tbUwagi";
            this.tbUwagi.Size = new System.Drawing.Size(236, 20);
            this.tbUwagi.TabIndex = 24;
            // 
            // tbIlosc
            // 
            this.tbIlosc.Location = new System.Drawing.Point(154, 101);
            this.tbIlosc.Name = "tbIlosc";
            this.tbIlosc.Size = new System.Drawing.Size(75, 20);
            this.tbIlosc.TabIndex = 23;
            this.tbIlosc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.keypress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 137);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "Informacje dodadkowe";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(110, 103);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "Ilość";
            // 
            // dodaj
            // 
            this.dodaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.dodaj.Location = new System.Drawing.Point(422, 101);
            this.dodaj.Name = "dodaj";
            this.dodaj.Size = new System.Drawing.Size(121, 53);
            this.dodaj.TabIndex = 20;
            this.dodaj.Text = "DODAJ POZYCJE";
            this.dodaj.UseVisualStyleBackColor = true;
            this.dodaj.Click += new System.EventHandler(this.dodaj_Click);
            // 
            // comboBoxDostawca
            // 
            this.comboBoxDostawca.FormattingEnabled = true;
            this.comboBoxDostawca.Location = new System.Drawing.Point(154, 33);
            this.comboBoxDostawca.Name = "comboBoxDostawca";
            this.comboBoxDostawca.Size = new System.Drawing.Size(236, 21);
            this.comboBoxDostawca.TabIndex = 19;
            this.comboBoxDostawca.SelectedIndexChanged += new System.EventHandler(this.comboBoxDostawca_SelectedIndexChanged);
            // 
            // tbOpisDokumentu
            // 
            this.tbOpisDokumentu.Location = new System.Drawing.Point(496, 67);
            this.tbOpisDokumentu.Name = "tbOpisDokumentu";
            this.tbOpisDokumentu.Size = new System.Drawing.Size(501, 20);
            this.tbOpisDokumentu.TabIndex = 33;
            // 
            // LabelOpisDokumentu
            // 
            this.LabelOpisDokumentu.AutoSize = true;
            this.LabelOpisDokumentu.Location = new System.Drawing.Point(406, 71);
            this.LabelOpisDokumentu.Name = "LabelOpisDokumentu";
            this.LabelOpisDokumentu.Size = new System.Drawing.Size(84, 13);
            this.LabelOpisDokumentu.TabIndex = 32;
            this.LabelOpisDokumentu.Text = "Opis dokumentu";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(454, 35);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 13);
            this.label6.TabIndex = 34;
            this.label6.Text = "Rabat";
            // 
            // LabelRabat
            // 
            this.LabelRabat.AutoSize = true;
            this.LabelRabat.Location = new System.Drawing.Point(507, 35);
            this.LabelRabat.Name = "LabelRabat";
            this.LabelRabat.Size = new System.Drawing.Size(36, 13);
            this.LabelRabat.TabIndex = 35;
            this.LabelRabat.Text = "Rabat";
            this.LabelRabat.Click += new System.EventHandler(this.LabelRabat_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.button2.Location = new System.Drawing.Point(572, 30);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(121, 23);
            this.button2.TabIndex = 36;
            this.button2.Text = "Udziel rabatu";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // doksprzedpoz_temp
            // 
            this.doksprzedpoz_temp.AllowUserToAddRows = false;
            this.doksprzedpoz_temp.AllowUserToDeleteRows = false;
            this.doksprzedpoz_temp.AutoGenerateColumns = false;
            this.doksprzedpoz_temp.BackgroundColor = System.Drawing.SystemColors.HighlightText;
            this.doksprzedpoz_temp.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.doksprzedpoz_temp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.doksprzedpoz_temp.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.doksprzedpozidDataGridViewTextBoxColumn,
            this.wytworidmDataGridViewTextBoxColumn,
            this.iloscDataGridViewTextBoxColumn,
            this.cenaDataGridViewTextBoxColumn,
            this.cenaporabacieDataGridViewTextBoxColumn,
            this.uwagiDataGridViewTextBoxColumn});
            this.doksprzedpoz_temp.DataSource = this.doksprzedpoztempBindingSource1;
            this.doksprzedpoz_temp.Location = new System.Drawing.Point(27, 196);
            this.doksprzedpoz_temp.Name = "doksprzedpoz_temp";
            this.doksprzedpoz_temp.ReadOnly = true;
            this.doksprzedpoz_temp.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.doksprzedpoz_temp.Size = new System.Drawing.Size(970, 312);
            this.doksprzedpoz_temp.TabIndex = 37;
            this.doksprzedpoz_temp.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.doksprzedpoz_temp_CellClick);
            // 
            // doksprzedpozidDataGridViewTextBoxColumn
            // 
            this.doksprzedpozidDataGridViewTextBoxColumn.DataPropertyName = "doksprzedpoz_id";
            this.doksprzedpozidDataGridViewTextBoxColumn.HeaderText = "doksprzedpoz_id";
            this.doksprzedpozidDataGridViewTextBoxColumn.Name = "doksprzedpozidDataGridViewTextBoxColumn";
            this.doksprzedpozidDataGridViewTextBoxColumn.ReadOnly = true;
            this.doksprzedpozidDataGridViewTextBoxColumn.Visible = false;
            // 
            // wytworidmDataGridViewTextBoxColumn
            // 
            this.wytworidmDataGridViewTextBoxColumn.DataPropertyName = "wytwor_idm";
            this.wytworidmDataGridViewTextBoxColumn.HeaderText = "Identyfikator";
            this.wytworidmDataGridViewTextBoxColumn.Name = "wytworidmDataGridViewTextBoxColumn";
            this.wytworidmDataGridViewTextBoxColumn.ReadOnly = true;
            this.wytworidmDataGridViewTextBoxColumn.Width = 200;
            // 
            // iloscDataGridViewTextBoxColumn
            // 
            this.iloscDataGridViewTextBoxColumn.DataPropertyName = "ilosc";
            this.iloscDataGridViewTextBoxColumn.HeaderText = "ilosc";
            this.iloscDataGridViewTextBoxColumn.Name = "iloscDataGridViewTextBoxColumn";
            this.iloscDataGridViewTextBoxColumn.ReadOnly = true;
            this.iloscDataGridViewTextBoxColumn.Width = 200;
            // 
            // cenaDataGridViewTextBoxColumn
            // 
            this.cenaDataGridViewTextBoxColumn.DataPropertyName = "cena";
            this.cenaDataGridViewTextBoxColumn.HeaderText = "cena";
            this.cenaDataGridViewTextBoxColumn.Name = "cenaDataGridViewTextBoxColumn";
            this.cenaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cenaporabacieDataGridViewTextBoxColumn
            // 
            this.cenaporabacieDataGridViewTextBoxColumn.DataPropertyName = "cena_po_rabacie";
            this.cenaporabacieDataGridViewTextBoxColumn.HeaderText = "cena po rabacie";
            this.cenaporabacieDataGridViewTextBoxColumn.Name = "cenaporabacieDataGridViewTextBoxColumn";
            this.cenaporabacieDataGridViewTextBoxColumn.ReadOnly = true;
            this.cenaporabacieDataGridViewTextBoxColumn.Width = 120;
            // 
            // uwagiDataGridViewTextBoxColumn
            // 
            this.uwagiDataGridViewTextBoxColumn.DataPropertyName = "uwagi";
            this.uwagiDataGridViewTextBoxColumn.HeaderText = "uwagi";
            this.uwagiDataGridViewTextBoxColumn.Name = "uwagiDataGridViewTextBoxColumn";
            this.uwagiDataGridViewTextBoxColumn.ReadOnly = true;
            this.uwagiDataGridViewTextBoxColumn.Width = 300;
            // 
            // doksprzedpoztempBindingSource1
            // 
            this.doksprzedpoztempBindingSource1.DataMember = "doksprzedpoz_temp";
            this.doksprzedpoztempBindingSource1.DataSource = this.w59018DataSet2;
            // 
            // w59018DataSet2
            // 
            this.w59018DataSet2.DataSetName = "w59018DataSet2";
            this.w59018DataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // doksprzedpoztempBindingSource
            // 
            this.doksprzedpoztempBindingSource.DataMember = "doksprzedpoz_temp";
            this.doksprzedpoztempBindingSource.DataSource = this.w59018DataSet2;
            // 
            // doksprzedpoz_tempTableAdapter
            // 
            this.doksprzedpoz_tempTableAdapter.ClearBeforeFill = true;
            // 
            // btAnulujRabat
            // 
            this.btAnulujRabat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.btAnulujRabat.Location = new System.Drawing.Point(722, 30);
            this.btAnulujRabat.Name = "btAnulujRabat";
            this.btAnulujRabat.Size = new System.Drawing.Size(121, 23);
            this.btAnulujRabat.TabIndex = 38;
            this.btAnulujRabat.Text = "Anuluj rabat";
            this.btAnulujRabat.UseVisualStyleBackColor = true;
            this.btAnulujRabat.Click += new System.EventHandler(this.btAnulujRabat_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bWstecz});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1063, 25);
            this.toolStrip1.TabIndex = 39;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // bWstecz
            // 
            this.bWstecz.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bWstecz.Image = ((System.Drawing.Image)(resources.GetObject("bWstecz.Image")));
            this.bWstecz.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bWstecz.Name = "bWstecz";
            this.bWstecz.Size = new System.Drawing.Size(48, 22);
            this.bWstecz.Text = "Wstecz";
            this.bWstecz.Click += new System.EventHandler(this.bWstecz_Click);
            // 
            // nowaFv
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1063, 535);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.btAnulujRabat);
            this.Controls.Add(this.doksprzedpoz_temp);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.LabelRabat);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbOpisDokumentu);
            this.Controls.Add(this.LabelOpisDokumentu);
            this.Controls.Add(this.btUsun);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbCena);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbUwagi);
            this.Controls.Add(this.tbIlosc);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dodaj);
            this.Controls.Add(this.comboBoxDostawca);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "nowaFv";
            this.Text = "Nowa Fv";
            this.Load += new System.EventHandler(this.nowaFv_Load);
            ((System.ComponentModel.ISupportInitialize)(this.doksprzedpoz_temp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.doksprzedpoztempBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.w59018DataSet2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.doksprzedpoztempBindingSource)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btUsun;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbCena;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbUwagi;
        private System.Windows.Forms.TextBox tbIlosc;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button dodaj;
        private System.Windows.Forms.ComboBox comboBoxDostawca;
        private System.Windows.Forms.TextBox tbOpisDokumentu;
        private System.Windows.Forms.Label LabelOpisDokumentu;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label LabelRabat;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridView doksprzedpoz_temp;
        private w59018DataSet2 w59018DataSet2;
        private System.Windows.Forms.BindingSource doksprzedpoztempBindingSource;
        private w59018DataSet2TableAdapters.doksprzedpoz_tempTableAdapter doksprzedpoz_tempTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn doksprzedpozidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn wytworidmDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iloscDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cenaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cenaporabacieDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn uwagiDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource doksprzedpoztempBindingSource1;
        private System.Windows.Forms.Button btAnulujRabat;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton bWstecz;
    }
}