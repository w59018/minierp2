﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace miniERP
{
    /// <summary>
    ///  Menu glowne programuu
    /// </summary>
    public partial class Form1 : Form
    {
    

        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Metoda odpowiedzialna za otworzenie formatki z Dokumenty PZ
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void bPz_Click(object sender, EventArgs e)
        {
             
            dokPz Pz = new dokPz();
            Pz.Show();
        }

        /// <summary>
        /// Metoda odpowiedzialna za otworzenie formatki z Dokumenty FV
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void bFv_Click(object sender, EventArgs e)
        {
             
            dokFv Fv = new dokFv();
            Fv.Show();
        }

        /// <summary>
        /// Metoda odpowiedzialna za otworzenie formatki z Kartoteki towarowe
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void bTowary_Click(object sender, EventArgs e)
        {
             
            dokTow Tow = new dokTow();
            Tow.Show();
        }

        /// <summary>
        /// Metoda odpowiedzialna za otworzenie formatki z Kontrahenci
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void bOdbiorcy_Click(object sender, EventArgs e)
        {
             
            dokOdb Odb = new dokOdb();
            Odb.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
