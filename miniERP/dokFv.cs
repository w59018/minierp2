﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Collections;
using System.Globalization;

namespace miniERP
{

    /// <summary>
    ///  Klasa dokumentów FV na którą składają się wszystkie funkcje zawarte w dokumentach sprzedaży.
    /// </summary>
    public partial class dokFv : Form
    {
        PolaczenieSQL pol = new PolaczenieSQL();
        string id;

        /// <summary>
        /// Odswiezenie polaczenia SQL, czynność wykonywana po operacji na bazie danych. 
        /// </summary>
        public void odswiezenie()
        {
            pol.polaczenieSQL();
            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM doksprzedpoz_temp", pol.con);
            DataSet ds = new DataSet();
            da.Fill(ds, "wytwor_idm");
            dataGridView1.DataSource = ds;
            dataGridView1.DataMember = "wytwor_idm";
            pol.con.Close();

        }

        /// <summary>
        /// Inicjalizacja komponentu dokFV
        /// </summary>
        public dokFv()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Metoda pobierająca dane po kliknięciu na tabelę faktur VAT
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">parametr przyjmujący informacje z tabeli bazy danych</param>
        private void listaOdb_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        /// <summary>
        /// Ten wiersz kodu wczytuje dane do tabeli 'w59018DataSet.doksprzed'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">parametr przesyła argumenty do pobrania danych z bazy</param>
        private void dokFv_Load(object sender, EventArgs e)
        {
            // TODO: Ten wiersz kodu wczytuje dane do tabeli 'w59018DataSet.doksprzed' . Możesz go przenieść lub usunąć.
            this.doksprzedTableAdapter.Fill(this.w59018DataSet.doksprzed);

        }

        /// <summary>
        /// Metoda wywoływana po wciśnięciu przycisku usuń, powoduje usunięcie pozycji z listy faktur
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void button1_Click(object sender, EventArgs e)
        {

            pol.polaczenieSQL();
            string query = "DELETE FROM doksprzedpoz_temp";
            pol.cmd = new SqlCommand(query, pol.con);
            pol.cmd.CommandType = CommandType.Text;
            try
            {
                pol.cmd.ExecuteNonQuery();
            }
            catch (SqlException blad)
            {
                if (blad.Errors.Count > 0)
                {
                    MessageBox.Show("Nie można usunąć pozycji!");
                    return;
                }

            }


            odswiezenie();
            pol.zakpolaczenieSQL();

            this.Hide();
            nowaFv nFv = new nowaFv();
            nFv.ShowDialog();
        }

        /// <summary>
        /// Metoda pobierająca dane z zaznaczonego obiektu w tabeli i przesyła go to textBox w formatce
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = this.dataGridView1.Rows[e.RowIndex];
            
            id = row.Cells["doksprzedidDataGridViewTextBoxColumn"].Value.ToString();
            pol.polaczenieSQL();
            string query = "SELECT nazwa FROM odbiorca WHERE odbiorca_id=" + id;
            pol.cmd = new SqlCommand(query, pol.con);
            pol.cmd.CommandType = CommandType.Text;
            pol.cmd.ExecuteNonQuery();
            string zwrotka = (string)pol.cmd.ExecuteScalar();
            tbOdbiorca.Text = zwrotka;
            tbIdentyfikator.Text = row.Cells["dokidmDataGridViewTextBoxColumn"].Value.ToString();
            tbWartDok.Text = row.Cells["wartdokDataGridViewTextBoxColumn"].Value.ToString();
        }

        /// <summary>
        /// Metoda wywołująca otwarcie nowego dokumentu 
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void otworzFv(object sender, DataGridViewCellMouseEventArgs e)
        {
            
            pol.polaczenieSQL();
            string query = "EXEC up_kpi_otworz_dok_fv " + id;
            //MessageBox.Show(query)
            pol.cmd = new SqlCommand(query, pol.con);
            pol.cmd.CommandType = CommandType.Text;
            try
            {
                pol.cmd.ExecuteNonQuery();
            }
            catch (SqlException blad)
            {
                if (blad.Errors.Count > 0)
                {
                    MessageBox.Show("Nie można otworzyć dokumentu!");
                    return;
                }

            }

            this.Hide();
            podFv nFv = new podFv();
            nFv.ShowDialog(); 
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void tbOdbiorca_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
