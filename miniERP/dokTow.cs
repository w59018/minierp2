﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Collections;
using System.Globalization;

namespace miniERP
{
    /// <summary>
    ///  Klasa kartotek towarowych.
    /// </summary>
    public partial class dokTow : Form


    {
        PolaczenieSQL pol = new PolaczenieSQL();
        string wytwor_id;
        
        /// <summary>
        /// Inizjalizowanie komponentu 
        /// </summary>
        public dokTow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Ten wiersz kodu wczytuje dane do tabeli 'w59018DataSet4.wytwor'
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void dokTow_Load(object sender, EventArgs e)
        {
            // TODO: Ten wiersz kodu wczytuje dane do tabeli 'w59018DataSet4.wytwor' . Możesz go przenieść lub usunąć.
            this.wytworTableAdapter2.Fill(this.w59018DataSet4.wytwor);
            // TODO: Ten wiersz kodu wczytuje dane do tabeli 'w59018DataSet3.wytwor' . Możesz go przenieść lub usunąć.
            this.wytworTableAdapter1.Fill(this.w59018DataSet3.wytwor);
            // TODO: Ten wiersz kodu wczytuje dane do tabeli 'w59018DataSet.wytwor' . Możesz go przenieść lub usunąć.
            this.wytworTableAdapter.Fill(this.w59018DataSet.wytwor);

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Metoda odpowiedzialna za otworzenie połączenia SQL i napełnienie tabel danymi
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            pol.polaczenieSQL();
            odswiezenie();
            DataGridViewRow row = this.listaTow.Rows[e.RowIndex];
            //tbNazwa.Text = row.Cells["wytworidDataGridViewTextBoxColumn"].Value.ToString();
            wytwor_id = row.Cells["wytworidDataGridViewTextBoxColumn"].Value.ToString();
            pol.con.Close();
        }

        /// <summary>
        /// Odswiezanie polaczenia SQL po zakończeniu pracy z bazą danych
        /// </summary>
        public void odswiezenie()
        {
            pol.polaczenieSQL();
            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM wytwor", pol.con);
            DataSet ds = new DataSet();
            da.Fill(ds, "wytwor");
            listaTow.DataSource = ds;
            listaTow.DataMember = "wytwor";
            pol.con.Close();

        }

        /// <summary>
        /// Metoda odpowiedzialna za dodawanie nowego produktu po wciśnięciu przycisku dodaj
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void button1_Click(object sender, EventArgs e)
        {

            

            if (tbIdentyfikator.Text != "" && tbNazwa.Text != "" && tbCena.Text != "" )
            {
                tbCena.Text = tbCena.Text.Replace(',', '.');
                pol.polaczenieSQL();


                string query = "SELECT COUNT(*) FROM wytwor WHERE wytwor_idm='" + tbIdentyfikator.Text + "'";
                pol.cmd = new SqlCommand(query, pol.con);
                pol.cmd.CommandType = CommandType.Text;
                pol.cmd.ExecuteNonQuery();
                int zwrotka = (int)pol.cmd.ExecuteScalar();

                if (zwrotka != 0)
                {
                    
                    string message1 = "Podany identyfikator " + tbIdentyfikator.Text + " już istnieje!";
                    string caption1 = "Nie można dodać pozycji!";
                    MessageBoxButtons buttons1 = MessageBoxButtons.OK;
                    DialogResult result1;
                    result1 = MessageBox.Show(message1, caption1, buttons1);
                    return;
                }



                pol.cmd = new SqlCommand("INSERT INTO wytwor (nazwa,uwagi,wytwor_idm,nazwa_rozw,censprzed) " +
                                 "VALUES	(@nazwa,@uwagi,@wytwor_idm,@nazwa_rozw,@censprzed)", pol.con);
                pol.cmd.Parameters.AddWithValue("@nazwa", tbNazwa.Text);
                pol.cmd.Parameters.AddWithValue("@uwagi", tbUwagi.Text);
                pol.cmd.Parameters.AddWithValue("@wytwor_idm", tbIdentyfikator.Text);
                pol.cmd.Parameters.AddWithValue("@nazwa_rozw", tbNazwaRozw.Text);
                pol.cmd.Parameters.AddWithValue("@censprzed", decimal.Parse(tbCena.Text, CultureInfo.InvariantCulture));
                pol.cmd.CommandType = CommandType.Text;
                pol.cmd.ExecuteNonQuery();

                string message = "Dodano nową pozycję";
                string caption = "Dodano pozycję";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result;

                tbNazwa.Text = "";
                tbUwagi.Text = "";
                tbNazwaRozw.Text = "";
                tbCena.Text = "";
                tbIdentyfikator.Text = "";
                
                result = MessageBox.Show(message, caption, buttons);

                odswiezenie();
            }
            else
            {
                string message = "Nie wypełniono wymaganych pól: Identyfikator, Nazwa, Cena !";
                string caption = "UWAGA";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result;
                result = MessageBox.Show(message, caption, buttons);

            }
        }

        private void tbIdentyfikator_TextChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Walidacja wprowadzanych danych z klawiatury i weryfikacja znaków jakie chce wprowadzić użytkownik [0,1,2,3,4,5,6,7,8,9...]
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void keypress(object sender, KeyPressEventArgs e)
        {
            char litera = e.KeyChar;
            
            if (!Char.IsDigit(litera) && litera != 8 && litera != 188 && litera != 46 && litera != 44)
            {
              e.Handled = true;
            }
        }

        /// <summary>
        /// Metoda odpowiedzialna za usunięcie pozycji po wciśnięciu przycisku usuwania pozycji
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void button2_Click(object sender, EventArgs e)
        {
            pol.polaczenieSQL();
            string query = "DELETE FROM wytwor WHERE wytwor_id = " + wytwor_id;
            //MessageBox.Show(query);
            pol.cmd = new SqlCommand(query, pol.con);
            pol.cmd.CommandType = CommandType.Text;
            try
            {
                pol.cmd.ExecuteNonQuery();
            }
            catch (SqlException blad)
            {
                if (blad.Errors.Count > 0)
                {
                    MessageBox.Show("Nie można usunąć towaru!");
                    return;
                }

            }


            odswiezenie();
            pol.zakpolaczenieSQL();
        }

        /// <summary>
        /// Metoda odpowiedzialna za pobranie danych z zaznaczonej pozycji na liscie
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void listaTow_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //pol.polaczenieSQL();
            //odswiezenie();
            DataGridViewRow row = this.listaTow.Rows[e.RowIndex];
            //tbNazwa.Text = row.Cells["wytworidDataGridViewTextBoxColumn"].Value.ToString();
            wytwor_id = row.Cells["wytworidDataGridViewTextBoxColumn"].Value.ToString();
            //pol.con.Close();

        }

        private void bWstecz_Click(object sender, EventArgs e)
        {

        }

        private void listaTow_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            

        }

        /// <summary>
        /// Metoda odpowiedzialna za aktualizację danych pozycji po wciśnięciu przycisku aktualizuj dane
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void button3_Click(object sender, EventArgs e)
        {
            if (tbIdentyfikator.Text != "" && tbNazwa.Text != "" && tbCena.Text != "" && wytwor_id != "")
            {
                tbCena.Text = tbCena.Text.Replace(',', '.');
                pol.polaczenieSQL();
                pol.cmd = new SqlCommand("UPDATE wytwor SET nazwa = @nazwa, uwagi = @uwagi, wytwor_idm = @wytwor_idm, nazwa_rozw = @nazwa_rozw, censprzed = @censprzed WHERE wytwor_id="+wytwor_id, pol.con);
                pol.cmd.Parameters.AddWithValue("@nazwa", tbNazwa.Text);
                pol.cmd.Parameters.AddWithValue("@uwagi", tbUwagi.Text);
                pol.cmd.Parameters.AddWithValue("@wytwor_idm", tbIdentyfikator.Text);
                pol.cmd.Parameters.AddWithValue("@nazwa_rozw", tbNazwaRozw.Text);
                pol.cmd.Parameters.AddWithValue("@censprzed", decimal.Parse(tbCena.Text, CultureInfo.InvariantCulture));
                pol.cmd.CommandType = CommandType.Text;
                pol.cmd.ExecuteNonQuery();

                string message = "Zaktualizowano pozycję";
                string caption = "Zaktualizowano pozycję";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result;

                tbNazwa.Text = "";
                tbUwagi.Text = "";
                tbNazwaRozw.Text = "";
                tbCena.Text = "";
                tbIdentyfikator.Text = "";

                result = MessageBox.Show(message, caption, buttons);

                odswiezenie();
            }
            else
            {
                string message = "Pola: Identyfikator, Nazwa, Cena nie mogą pozostać puste!";
                string caption = "UWAGA";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result;
                result = MessageBox.Show(message, caption, buttons);

            }

        }

        /// <summary>
        /// Metoda odpowiedzialna za pobieranie danych z zaznaczonej pozycji na liscie
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void listaTow_CellContentClick_2(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = this.listaTow.Rows[e.RowIndex];
            tbIdentyfikator.Text = row.Cells["wytworidmDataGridViewTextBoxColumn"].Value.ToString();
            tbNazwa.Text = row.Cells["nazwaDataGridViewTextBoxColumn"].Value.ToString();
            tbNazwaRozw.Text = row.Cells["nazwarozwDataGridViewTextBoxColumn"].Value.ToString();
            tbCena.Text = row.Cells["censprzedDataGridViewTextBoxColumn"].Value.ToString();
            tbUwagi.Text = row.Cells["uwagiDataGridViewTextBoxColumn"].Value.ToString();
            wytwor_id = row.Cells["wytworidDataGridViewTextBoxColumn"].Value.ToString();
        }
    }
}
