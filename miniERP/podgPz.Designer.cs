﻿namespace miniERP
{
    partial class nowyPZ
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(nowyPZ));
            this.comboBoxDostawca = new System.Windows.Forms.ComboBox();
            this.dodaj = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbIlosc = new System.Windows.Forms.TextBox();
            this.tbUwagi = new System.Windows.Forms.TextBox();
            this.tbCena = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.btUsun = new System.Windows.Forms.Button();
            this.dokdostpozBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.w59018DataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.w59018DataSet = new miniERP.w59018DataSet();
            this.dokdostpozTableAdapter = new miniERP.w59018DataSetTableAdapters.dokdostpozTableAdapter();
            this.w59018DataSet1 = new miniERP.w59018DataSet1();
            this.dokdostpoztempBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dokdostpoz_tempTableAdapter = new miniERP.w59018DataSet1TableAdapters.dokdostpoz_tempTableAdapter();
            this.dokdostpoz_tem = new System.Windows.Forms.DataGridView();
            this.dokdostpoztempBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.tbOpisDokumentu = new System.Windows.Forms.TextBox();
            this.LabelOpisDokumentu = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.Wstecz = new System.Windows.Forms.ToolStripButton();
            this.dokdostpozidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wytworidmDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iloscDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cenaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uwagiDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dokdostpozBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.w59018DataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.w59018DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.w59018DataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dokdostpoztempBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dokdostpoz_tem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dokdostpoztempBindingSource1)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBoxDostawca
            // 
            this.comboBoxDostawca.FormattingEnabled = true;
            this.comboBoxDostawca.Location = new System.Drawing.Point(162, 30);
            this.comboBoxDostawca.Name = "comboBoxDostawca";
            this.comboBoxDostawca.Size = new System.Drawing.Size(236, 21);
            this.comboBoxDostawca.TabIndex = 4;
            this.comboBoxDostawca.SelectedIndexChanged += new System.EventHandler(this.comboBoxDostawca_SelectedIndexChanged);
            // 
            // dodaj
            // 
            this.dodaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.dodaj.Location = new System.Drawing.Point(430, 98);
            this.dodaj.Name = "dodaj";
            this.dodaj.Size = new System.Drawing.Size(121, 53);
            this.dodaj.TabIndex = 7;
            this.dodaj.Text = "DODAJ POZYCJE";
            this.dodaj.UseVisualStyleBackColor = true;
            this.dodaj.Click += new System.EventHandler(this.dodaj_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(118, 100);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Ilość";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 134);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Informacje dodadkowe";
            // 
            // tbIlosc
            // 
            this.tbIlosc.Location = new System.Drawing.Point(162, 98);
            this.tbIlosc.Name = "tbIlosc";
            this.tbIlosc.Size = new System.Drawing.Size(75, 20);
            this.tbIlosc.TabIndex = 10;
            // 
            // tbUwagi
            // 
            this.tbUwagi.Location = new System.Drawing.Point(162, 131);
            this.tbUwagi.Name = "tbUwagi";
            this.tbUwagi.Size = new System.Drawing.Size(236, 20);
            this.tbUwagi.TabIndex = 11;
            // 
            // tbCena
            // 
            this.tbCena.Location = new System.Drawing.Point(323, 98);
            this.tbCena.Name = "tbCena";
            this.tbCena.Size = new System.Drawing.Size(75, 20);
            this.tbCena.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(270, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Cena";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(92, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Dostawca";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(103, 66);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Produkt";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.button1.Location = new System.Drawing.Point(740, 98);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(275, 53);
            this.button1.TabIndex = 16;
            this.button1.Text = "ZATWIERDŹ DOKUMENT";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(162, 57);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(236, 21);
            this.comboBox1.TabIndex = 6;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(162, 63);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(236, 21);
            this.comboBox2.TabIndex = 17;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // btUsun
            // 
            this.btUsun.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.btUsun.Location = new System.Drawing.Point(584, 98);
            this.btUsun.Name = "btUsun";
            this.btUsun.Size = new System.Drawing.Size(121, 53);
            this.btUsun.TabIndex = 18;
            this.btUsun.Text = "USUŃ POZYCJE";
            this.btUsun.UseVisualStyleBackColor = true;
            this.btUsun.Click += new System.EventHandler(this.button2_Click);
            // 
            // dokdostpozBindingSource
            // 
            this.dokdostpozBindingSource.DataMember = "dokdostpoz";
            this.dokdostpozBindingSource.DataSource = this.w59018DataSetBindingSource;
            // 
            // w59018DataSetBindingSource
            // 
            this.w59018DataSetBindingSource.DataSource = this.w59018DataSet;
            this.w59018DataSetBindingSource.Position = 0;
            // 
            // w59018DataSet
            // 
            this.w59018DataSet.DataSetName = "w59018DataSet";
            this.w59018DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dokdostpozTableAdapter
            // 
            this.dokdostpozTableAdapter.ClearBeforeFill = true;
            // 
            // w59018DataSet1
            // 
            this.w59018DataSet1.DataSetName = "w59018DataSet1";
            this.w59018DataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dokdostpoztempBindingSource
            // 
            this.dokdostpoztempBindingSource.DataMember = "dokdostpoz_temp";
            this.dokdostpoztempBindingSource.DataSource = this.w59018DataSet1;
            // 
            // dokdostpoz_tempTableAdapter
            // 
            this.dokdostpoz_tempTableAdapter.ClearBeforeFill = true;
            // 
            // dokdostpoz_tem
            // 
            this.dokdostpoz_tem.AllowUserToAddRows = false;
            this.dokdostpoz_tem.AllowUserToDeleteRows = false;
            this.dokdostpoz_tem.AutoGenerateColumns = false;
            this.dokdostpoz_tem.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dokdostpoz_tem.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dokdostpoz_tem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dokdostpoz_tem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dokdostpozidDataGridViewTextBoxColumn,
            this.wytworidmDataGridViewTextBoxColumn,
            this.iloscDataGridViewTextBoxColumn,
            this.cenaDataGridViewTextBoxColumn,
            this.uwagiDataGridViewTextBoxColumn});
            this.dokdostpoz_tem.DataSource = this.dokdostpoztempBindingSource1;
            this.dokdostpoz_tem.Location = new System.Drawing.Point(35, 184);
            this.dokdostpoz_tem.Name = "dokdostpoz_tem";
            this.dokdostpoz_tem.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dokdostpoz_tem.Size = new System.Drawing.Size(980, 325);
            this.dokdostpoz_tem.TabIndex = 20;
            this.dokdostpoz_tem.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dokdostpoz_tem_click);
            this.dokdostpoz_tem.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dokdostpoz_temp);
            // 
            // dokdostpoztempBindingSource1
            // 
            this.dokdostpoztempBindingSource1.DataMember = "dokdostpoz_temp";
            this.dokdostpoztempBindingSource1.DataSource = this.w59018DataSet1;
            // 
            // tbOpisDokumentu
            // 
            this.tbOpisDokumentu.Location = new System.Drawing.Point(514, 30);
            this.tbOpisDokumentu.Name = "tbOpisDokumentu";
            this.tbOpisDokumentu.Size = new System.Drawing.Size(501, 20);
            this.tbOpisDokumentu.TabIndex = 22;
            // 
            // LabelOpisDokumentu
            // 
            this.LabelOpisDokumentu.AutoSize = true;
            this.LabelOpisDokumentu.Location = new System.Drawing.Point(424, 34);
            this.LabelOpisDokumentu.Name = "LabelOpisDokumentu";
            this.LabelOpisDokumentu.Size = new System.Drawing.Size(84, 13);
            this.LabelOpisDokumentu.TabIndex = 21;
            this.LabelOpisDokumentu.Text = "Opis dokumentu";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Wstecz});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1063, 25);
            this.toolStrip1.TabIndex = 23;
            this.toolStrip1.Text = "toolStrip1";
            this.toolStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStrip1_ItemClicked);
            // 
            // Wstecz
            // 
            this.Wstecz.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.Wstecz.Image = ((System.Drawing.Image)(resources.GetObject("Wstecz.Image")));
            this.Wstecz.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Wstecz.Name = "Wstecz";
            this.Wstecz.Size = new System.Drawing.Size(48, 22);
            this.Wstecz.Text = "Wstecz";
            this.Wstecz.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // dokdostpozidDataGridViewTextBoxColumn
            // 
            this.dokdostpozidDataGridViewTextBoxColumn.DataPropertyName = "dokdostpoz_id";
            this.dokdostpozidDataGridViewTextBoxColumn.HeaderText = "dokdostpoz_id";
            this.dokdostpozidDataGridViewTextBoxColumn.Name = "dokdostpozidDataGridViewTextBoxColumn";
            this.dokdostpozidDataGridViewTextBoxColumn.ReadOnly = true;
            this.dokdostpozidDataGridViewTextBoxColumn.Visible = false;
            // 
            // wytworidmDataGridViewTextBoxColumn
            // 
            this.wytworidmDataGridViewTextBoxColumn.DataPropertyName = "wytwor_idm";
            this.wytworidmDataGridViewTextBoxColumn.HeaderText = "Identyfikator";
            this.wytworidmDataGridViewTextBoxColumn.Name = "wytworidmDataGridViewTextBoxColumn";
            this.wytworidmDataGridViewTextBoxColumn.ReadOnly = true;
            this.wytworidmDataGridViewTextBoxColumn.Width = 300;
            // 
            // iloscDataGridViewTextBoxColumn
            // 
            this.iloscDataGridViewTextBoxColumn.DataPropertyName = "ilosc";
            this.iloscDataGridViewTextBoxColumn.HeaderText = "ilosc";
            this.iloscDataGridViewTextBoxColumn.Name = "iloscDataGridViewTextBoxColumn";
            this.iloscDataGridViewTextBoxColumn.ReadOnly = true;
            this.iloscDataGridViewTextBoxColumn.Width = 200;
            // 
            // cenaDataGridViewTextBoxColumn
            // 
            this.cenaDataGridViewTextBoxColumn.DataPropertyName = "cena";
            this.cenaDataGridViewTextBoxColumn.HeaderText = "cena";
            this.cenaDataGridViewTextBoxColumn.Name = "cenaDataGridViewTextBoxColumn";
            this.cenaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // uwagiDataGridViewTextBoxColumn
            // 
            this.uwagiDataGridViewTextBoxColumn.DataPropertyName = "uwagi";
            this.uwagiDataGridViewTextBoxColumn.HeaderText = "uwagi";
            this.uwagiDataGridViewTextBoxColumn.Name = "uwagiDataGridViewTextBoxColumn";
            this.uwagiDataGridViewTextBoxColumn.ReadOnly = true;
            this.uwagiDataGridViewTextBoxColumn.Width = 320;
            // 
            // nowyPZ
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1063, 535);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.tbOpisDokumentu);
            this.Controls.Add(this.LabelOpisDokumentu);
            this.Controls.Add(this.dokdostpoz_tem);
            this.Controls.Add(this.btUsun);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbCena);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbUwagi);
            this.Controls.Add(this.tbIlosc);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dodaj);
            this.Controls.Add(this.comboBoxDostawca);
            this.Name = "nowyPZ";
            this.Text = "Dokument PZ";
            this.Load += new System.EventHandler(this.nowyPZ_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dokdostpozBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.w59018DataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.w59018DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.w59018DataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dokdostpoztempBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dokdostpoz_tem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dokdostpoztempBindingSource1)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox comboBoxDostawca;
        private System.Windows.Forms.Button dodaj;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbIlosc;
        private System.Windows.Forms.TextBox tbUwagi;
        private System.Windows.Forms.TextBox tbCena;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Button btUsun;
        private System.Windows.Forms.BindingSource w59018DataSetBindingSource;
        private w59018DataSet w59018DataSet;
        private System.Windows.Forms.BindingSource dokdostpozBindingSource;
        private w59018DataSetTableAdapters.dokdostpozTableAdapter dokdostpozTableAdapter;
        private w59018DataSet1 w59018DataSet1;
        private System.Windows.Forms.BindingSource dokdostpoztempBindingSource;
        private w59018DataSet1TableAdapters.dokdostpoz_tempTableAdapter dokdostpoz_tempTableAdapter;
        private System.Windows.Forms.DataGridView dokdostpoz_tem;
        private System.Windows.Forms.BindingSource dokdostpoztempBindingSource1;
        private System.Windows.Forms.TextBox tbOpisDokumentu;
        private System.Windows.Forms.Label LabelOpisDokumentu;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton Wstecz;
        private System.Windows.Forms.DataGridViewTextBoxColumn dokdostpozidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn wytworidmDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iloscDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cenaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn uwagiDataGridViewTextBoxColumn;
    }
}